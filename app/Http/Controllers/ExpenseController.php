<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class ExpenseController extends Controller
{
    private $expenseCategories;
    private $paymentMethods;
    private $rules = [];

    public function __construct()
    {
        $this->expenseCategories = config('expense.expense_category');
        $this->paymentMethods = config('expense.payment_method');

        $this->rules = [
            'description' => ['required', 'min:3'],
            'date' => ['required', 'date'],
            'amount' => ['required', 'min:1'],
            'category' => ['required', Rule::in($this->expenseCategories)],
            'payment_method' => ['required', Rule::in($this->paymentMethods)],
        ];
    }

    public function index()
    {
        $expenses = Expense::orderByDesc('id')
            ->paginate(10);

        return Inertia::render('Expenses/index', [
            'expenses' => $expenses,
        ]);
    }

    public function add()
    {
        return Inertia::render('Expenses/add/index')
            ->with('expense', new Expense)
            ->with('expensesCategories', $this->expenseCategories)
            ->with('paymentMethods', $this->paymentMethods);
    }

    public function store(Request $request)
    {

        $postData = $this->validate($request, $this->rules);

        $postData['user_id'] = Auth::user()->id;

        Expense::create($postData);

        return redirect(route('expense.list'))
            ->with('success', 'New Expense Added Successfully 💸');
    }

    public function view(Expense $expense)
    {
        return Inertia::render('Expenses/view/index', [
            'expense' => $expense,
            'expensesCategories' => $this->expenseCategories,
            'paymentMethods' => $this->paymentMethods,
        ]);
    }

    public function update(Request $request)
    {
        // Todo: handle the update logic for expenses not belong to user

        $this->rules['id'] = ['required', 'exists:expenses,id'];

        $postData = $this->validate($request, $this->rules);

        $expenseId = $postData['id'];
        unset($postData['id']); //destroys the specified variables.

        Expense::where('id', $expenseId)
            ->update($postData);

        return redirect(route('expense.list'))
            ->with('success', 'Expense Updated Successfully 👍');
    }

    public function delete(Request $request)
    {
        $expenseId = $request->input('id');

        if ($request->input('user_id') !== Auth::user()->id) {
            abort(401, 'This expense belong to another user, you can\'t delete it ');
        }

        DB::table('expenses')->where('id', $expenseId)->delete();

        return redirect()->route('expense.list');
    }
}
