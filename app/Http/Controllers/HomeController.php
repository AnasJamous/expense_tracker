<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Inertia\Inertia;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $expenses = Expense::orderByDesc('id')
            ->paginate(10);

        return Inertia::render('Expenses/index', [
            'expenses' => $expenses,
        ]);
        // return view('home');
    }
}
