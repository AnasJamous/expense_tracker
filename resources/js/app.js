require('./bootstrap');

import { InertiaApp } from '@inertiajs/inertia-react'
import React from 'react'
import { render } from 'react-dom'
// import route from 'ziggy';
// import { Ziggy } from './ziggy';

const app = document.getElementById('app')
// route('', undefined, undefined, Ziggy);

render(
  <InertiaApp
    initialPage={JSON.parse(app.dataset.page)}
    resolveComponent={name => require(`./Pages/${name}`).default}
  />,
  app
)