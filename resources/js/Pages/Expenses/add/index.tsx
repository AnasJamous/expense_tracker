import React from "react";
import Layout from "../../../components/common/layout";
import ExpenseForm from "../../../components/forms/expenseform";
import Expense from "../../../interface/Expense";

interface Props {
  expense: Expense;
  expensesCategories: Array<any>;
  paymentMethods: Array<any>;
}

const ExpenseAddPage: React.FC<Props> = ({
  expense,
  expensesCategories,
  paymentMethods
}) => {
  return (
    <Layout pageTitle="Add Expense">
      <div className="row">
        <div className="col-md-6">
          <div className="card">
            <div className="card-header">Add expense</div>
            <div className="card-body">
              <ExpenseForm
                expense={expense}
                expenseCategories={expensesCategories}
                paymentMethods={paymentMethods}
                submitUrl="/expense/save"
              />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default ExpenseAddPage;
