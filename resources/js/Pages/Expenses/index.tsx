import React from "react";
import { Inertia } from "@inertiajs/inertia";
import Expense from "../../interface/Expense";
import { confirmAlert } from "react-confirm-alert";
import Layout from "../../components/common/layout";
import { InertiaLink } from "@inertiajs/inertia-react";
import "react-confirm-alert/src/react-confirm-alert.css";
import PaginatedData from "../../interface/PaginatedData";
import Pagination from "../../components/common/pagination";

interface Props {
  expenses: PaginatedData;
}

const ExpenseListPage: React.FC<Props> = ({ expenses }) => {
  const { data } = expenses;
  const handleExpenseDelete = (expense: Expense) => {
    console.log(expense);

    confirmAlert({
      title: "Delete an expense",
      message: "Are you sure you want to delete this expense 🙄 ?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            Inertia.post("/expense/delete", expense);
          }
        },
        {
          label: "No",
          onClick: () => {}
        }
      ]
    });
  };

  return (
    <Layout pageTitle="My Expense List">
      <div className="row">
        <div className="col-md-12">
          <div className="card mb-3">
            <div className="card-header">My Expenses</div>
            <table className="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Description</th>
                  <th>Category</th>
                  <th>Payment Method</th>
                  <th>Date</th>
                  <th>View/Delete</th>
                </tr>
              </thead>
              <tbody>
                {data.length > 0 &&
                  data.map((expense: Expense, index: any) => {
                    return (
                      <tr key={index}>
                        <td> {expense.id} </td>
                        <td> {expense.description} </td>
                        <td> {expense.category} </td>
                        <td> {expense.payment_method} </td>
                        <td> {expense.date} </td>
                        <td>
                          <InertiaLink
                            href={`/expense/view/${expense.id}`}
                            className="mr-3 text-success"
                          >
                            View
                          </InertiaLink>
                          <span
                            className="pointer"
                            onClick={() => handleExpenseDelete(expense)}
                          >
                            Delete
                          </span>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
          <Pagination links={expenses.links} />
        </div>
      </div>
    </Layout>
  );
};

export default ExpenseListPage;
