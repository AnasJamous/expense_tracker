import React from "react";
import Layout from "../../components/common/layout";
import Expense from "../../interface/Expense";

interface Props {
  expenses: Array<Expense>;
}

const HomePage = (props: Props) => {
  const { expenses } = props;
  return (
    <Layout pageTitle="My Expenses">
      <div>
        <p>Welcome to home</p>
        <ul>
          {expenses.map((expense: Expense, index) => {
            return <li key={index}>{expense.description}</li>;
          })}
        </ul>
      </div>
    </Layout>
  );
};

export default HomePage;
