const Ziggy = {"url":"http:\/\/localhost","port":null,"defaults":[],"routes":{"login":{"uri":"login","methods":["GET","HEAD"]},"logout":{"uri":"logout","methods":["POST"]},"register":{"uri":"register","methods":["GET","HEAD"]},"password.request":{"uri":"password\/reset","methods":["GET","HEAD"]},"password.email":{"uri":"password\/email","methods":["POST"]},"password.reset":{"uri":"password\/reset\/{token}","methods":["GET","HEAD"]},"password.update":{"uri":"password\/reset","methods":["POST"]},"password.confirm":{"uri":"password\/confirm","methods":["GET","HEAD"]},"home":{"uri":"home","methods":["GET","HEAD"]},"expense.list":{"uri":"expenses","methods":["GET","HEAD"]},"expense.add":{"uri":"expense\/add","methods":["GET","HEAD"]},"expense.save":{"uri":"expense\/save","methods":["POST"]},"expense.view":{"uri":"expense\/view\/{expense}","methods":["GET","HEAD"],"bindings":{"expense":"id"}},"expense.update":{"uri":"expense\/update","methods":["POST"]},"expense.delete":{"uri":"expense\/delete\/{expense}","methods":["GET","HEAD"],"bindings":{"expense":"id"}}}};

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    for (let name in window.Ziggy.routes) {
        Ziggy.routes[name] = window.Ziggy.routes[name];
    }
}

export { Ziggy };
